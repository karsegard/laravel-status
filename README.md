# laravel-status

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/laravel-status.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-status)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/laravel-status.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-status)

## Installation

You can install the package via composer:

```bash
composer require kda/laravel-status
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="\KDA\Laravel\Status\ServiceProvider" --tag="migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="\KDA\Laravel\Status\ServiceProvider" --tag="config"
```


## Usage
wip
```php

```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://github.com/fdt2k/.github/blob/main/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://gitlab.com/fdt2k)
- [All Contributors](contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
