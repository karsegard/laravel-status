Feature: Initial status
    A status marked as initial is set as default when creating a model
    Background: Status are created
        Given the following factory attributes 
        """
        {
            "group":"post",
            "name":"published"
        }
        """
        And crafting a record "KDA\Laravel\Status\Models\Status" 
        Given the following factory attributes 
        """
        {
            "group":"post",
            "name":"draft",
            "initial":true
        }
        """
        And crafting a record "KDA\Laravel\Status\Models\Status" 
        And clearing factory attributes

    Scenario: 
        Given crafting a record "KDA\Tests\Models\Post" 
        Then the record is not null
        And we have "1" records of model "KDA\Laravel\Status\Models\StatusHistory" 
        And the record property "status" is not null
        And the record path "status.id" is "1" 

       

  
