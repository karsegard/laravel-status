Feature: Status Factories

    Scenario: Status can be created
    Given the following factory attributes 
    """
    {
        "name":"test"
    }
    """
    And crafting a record "KDA\Laravel\Status\Models\Status" 
    Then The record is not null

    Scenario: Status can be created
    Given crafting a record "KDA\Laravel\Status\Models\Status" 
    Then The record is not null

    Scenario: History can be created
    Given crafting a record "KDA\Laravel\Status\Models\StatusHistory" 
    Then The record is not null
