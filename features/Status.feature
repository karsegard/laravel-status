Feature: Managing status

    Scenario: We can create several status
        Given crafting a record "KDA\Laravel\Status\Models\Status" 
        And the factory attribute "group" with value "invoice"
        And crafting "3" records "KDA\Laravel\Status\Models\Status" 
        Then we have "4" records of model "KDA\Laravel\Status\Models\Status" 
    
    Scenario: We can create several status
        Given crafting a record "KDA\Laravel\Status\Models\Status" 
        And the factory attribute "group" with value "invoice"
        And crafting "3" records "KDA\Laravel\Status\Models\Status" 
        Given a query on "KDA\Laravel\Status\Models\Status" 
        And with scope "group" with "invoice"
        Then there is "3" query results
   
    Scenario: There is no initial status in database
        Given crafting a record "KDA\Tests\Models\Post" 
        And the record is not null
        Then we have "0" records of model "KDA\Laravel\Status\Models\StatusHistory" 

    Scenario: We have a model with no status group defined
        Given crafting a record "KDA\Tests\Models\PostWithNoStatusGroup" 
        And the record is not null
        Then we have "0" records of model "KDA\Laravel\Status\Models\StatusHistory" 

  
