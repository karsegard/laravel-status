<?php

namespace KDA\Laravel\Status;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Status\Models\Contracts\StatusGroup;
use KDA\Laravel\Status\Models\Status as ModelsStatus;
use KDA\Laravel\Status\Models\StatusHistory;

//use Illuminate\Support\Facades\Blade;
class Status
{
    protected $registeredModels=[];

    public function getDefaultStatusForModel($model,$group=''):?ModelsStatus
    {
        return  ModelsStatus::group($group)
        ->where('initial', true)
        ->first();
    }
    public function createStatusForModel($model)
    {
        $group = $this->getGroupForModel($model);
       // if (!blank($group)) {
            $initialStatus = $this->getDefaultStatusForModel($model,$group);

            if ($initialStatus) {
                StatusHistory::create([
                    'model_id' => $model->getKey(),
                    'model_type' => get_class($model),
                    'status_id' => $initialStatus->getKey(),
                    'user_id' => auth()
                        ?->user()
                        ?->getKey(),
                ]);
            }
        //}
    }

    public function updateStatusForModel($model, $status)
    {
        StatusHistory::create([
            'model_id' => $model->getKey(),
            'model_type' => get_class($model),
            'status_id' => $status->getKey(),
            'user_id' => auth()
                ?->user()
                ?->getKey(),
        ]);
    }

    public function setStatus(Model $model, ModelsStatus|string | int $value,$group='')
    {
        if (is_string($value)) { // why ?? refactor
            $group = $this->getGroupForModel($model);

            $status = ModelsStatus::group($group)->where('key',$value)->first();
            if($status){
                Status::updateStatusForModel($model, $status);
            }
        }else if ( is_int($value) ){
            $status = $this->findStatusById($value);
            if($status){
                Status::updateStatusForModel($model, $status);
            }
        }
        else if($value instanceof ModelsStatus){
            Status::updateStatusForModel($model, $value);
        }
    }

    public function findStatusById($id):ModelsStatus
    {
        return ModelsStatus::find($id);
    }

    public function getStatus(?Model $model)
    {
        return $model?->lastStatusHistory?->status;
    }

    public function registerModel(string $model,$group='', $withGlobalScope = true,$withHooks=true)
    {
        $this->registeredModels[$model]=$group;
        $model::resolveRelationUsing('statusHistory', function ($orderModel) {
            return $orderModel->morphOne(StatusHistory::class, 'model');
        });
        $model::resolveRelationUsing('lastStatusHistory', function ($orderModel) {
            return $orderModel->morphOne(StatusHistory::class, 'model')->latestOfMany();
        });

        if ($withGlobalScope) {
            $model::addGlobalScope('withoutFinished', function (Builder $q) {
                return $q->whereHas('lastStatusHistory', function ($q) {
                    $q->whereHas('status',function($q){
                        $q->where('final', false);
                    });
                });
            });
        }
        if ($withHooks) {
            $model::created(function ($model) {
                $this->createStatusForModel($model);
            });
        }
    }

    public function isRegisteredModel(string | Model $model){
        $model =($model instanceof Model) ? get_class($model):$model;
        
        return isset($this->registeredModels[$model]);
    }
    public function getRegisteredModelGroup(string | Model $model){
        $model =($model instanceof Model) ? get_class($model):$model;
        
        return $this->registeredModels[$model];
    }

    public function getGroupForModel(Model $model){
        $group = '';
        if($model instanceof StatusGroup){
            $group = $model->getStatusGroup() ;
        }else if($this->isRegisteredModel($model)){
            $group = $this->getRegisteredModelGroup($model);
        }
        return $group;
    }
}
