<?php
namespace KDA\Laravel\Status;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Status\Facades\Status as Facade;
use KDA\Laravel\Status\Status as Library;
use KDA\Laravel\Traits\HasDumps;
use KDA\Laravel\Traits\HasConfigurableTableNames;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasMigration;
    use HasConfigurableTableNames;
    use HasDumps;
    protected $packageName ='laravel-status';
    public function getDumps():array{
        return config(self::$tables_config_key);
    }
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
     // trait \KDA\Laravel\Traits\HasConfig; 
     //    registers config file as 
     //      [file_relative_to_config_dir => namespace]
    protected $configDir='config';
    protected $configs = [
         'kda/status.php'  => 'kda.laravel-status'
    ];
    protected static $tables_config_key = 'kda.laravel-status.tables';
    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
