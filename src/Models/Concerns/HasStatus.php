<?php

namespace KDA\Laravel\Status\Models\Concerns;

use KDA\Laravel\Status\Facades\Status;
use KDA\Laravel\Status\Models\Status as ModelsStatus;
use KDA\Laravel\Status\Models\StatusHistory;
use KDA\Laravel\Status\Models\Contracts\StatusGroup;

trait HasStatus
{
    public static function bootHasStatus(): void
    {
        static::created(function ($model) {
            Status::createStatusForModel($model);
        });
        static::updating(function ($model) {});
    }

    public function statusHistory()
    {
        return $this->morphMany(StatusHistory::class, 'model');
    }

    public function lastStatusHistory()
    {
        return $this->morphOne(StatusHistory::class, 'model')->latestOfMany();
    }

    public function getStatusAttribute(): ?ModelsStatus
    {
        return $this->lastStatusHistory?->status;
    }

    public function setStatusAttribute(ModelStatus|string $value)
    {
      /*  if (is_string($value)) {
            $hasGroup = $this instanceof StatusGroup;
            $group = $hasGroup ? $this->getStatusGroup() : '';
            $status = ModelsStatus::group($group)->where('key',$value)->first();
            if($status){
                $this->setStatus($status);
            }
        }else if($value instanceof ModelsStatus){
            $this->setStatus($status);
        }*/
        Status::setStatus($model,$value);
    }

    public function setStatus(ModelsStatus $status)
    {
        Status::updateStatusForModel($this, $status);
    }


    public function scopeStatusNotFinished($q){
       /* return $q->whereHas('lastStatus',function($q){
            $q->notFinal();
        });*/
        return $q->whereHas('lastStatusHistory',function($q){
            return $q->whereHas('status',function($q){
                $q->notFinal();
            });
        });
    }
    public function scopeStatusFinished($q){
        /* return $q->whereHas('lastStatus',function($q){
             $q->notFinal();
         });*/
         return $q->whereHas('lastStatusHistory',function($q){
             return $q->whereHas('status',function($q){
                 $q->final();
             });
         });
     }

    public function scopeStatusNotStarted($q){
        /* return $q->whereHas('lastStatus',function($q){
             $q->notFinal();
         });*/
         return $q->whereHas('lastStatusHistory',function($q){
             return $q->whereHas('status',function($q){
                 $q->initial();
             });
         });
     }

     public function scopeStatusStarted($q){
        /* return $q->whereHas('lastStatus',function($q){
             $q->notFinal();
         });*/
         return $q->whereHas('lastStatusHistory',function($q){
             return $q->whereHas('status',function($q){
                 $q->notFinal()->notInitial();
             });
         });
     }

     public function scopeWithStatus($q,$status){
        if(!($status instanceof ModelsStatus)){
            $status = ModelsStatus::find($status);
        }
        return $q->whereHas('lastStatusHistory',function($q) use ($status){
           return $q->where('status_id',$status->getKey());
        });
     }

     public function scopeWithAnyStatus($q,$statuses){
        $statuses = collect($statuses)->map(function($status){
            if(!($status instanceof ModelsStatus)){
                $status = ModelsStatus::find($status);
            }
            return $status;
        })->all();
        return $q->whereHas('lastStatusHistory',function($q) use ($statuses){
            return $q->where(function($q) use ($statuses){
                foreach($statuses as $status){
                   $q->orWhere('status_id',$status->getKey());
                }
            });
         });
     }

}
