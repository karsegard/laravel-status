<?php

namespace KDA\Laravel\Status\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Laravel\Status\Database\Factories\StatusHistoryFactory;

class StatusHistory extends Model
{
    use HasFactory;

    protected $table = 'kda_status_histories';

    protected $fillable = [
        'status_id', 'user_id', 'model_id', 'model_type'
    ];

    protected $appends = [];

    protected $casts = [];


    protected static function newFactory()
    {
        return  StatusHistoryFactory::new();
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function scopeStatusNotFinished($q)
    {
        return $q->whereHas('status', function ($q) {
            $q->notFinal();
        });
    }
}
