<?php

namespace KDA\Laravel\Status\Models\Contracts;


interface StatusGroup
{

    public function getStatusGroup():string;
}
