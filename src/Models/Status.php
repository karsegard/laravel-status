<?php

namespace KDA\Laravel\Status\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;
use \KDA\Laravel\Status\Database\Factories\StatusFactory;

class Status extends Model
{
    use HasFactory;
    use HasDefaultAttributes;
    protected $table='kda_status';

    protected $fillable = [
        'name','meta','group','text_color','bg_color','owner_id','owner_type','initial','final','key','sort'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       'meta'=>'json',
       'group'=>'json'
    ];

   
    protected static function newFactory()
    {
        return StatusFactory::new();
    }

    public function createDefaultAttributes()
    {
        $this->defaultAttribute('key',\Str::slug($this->attributes['name']));
        $this->defaultAttribute('name',\Str::camel($this->attributes['key']));
    }

    public static function createOwned($owner,$group,$name,$text_color,$background_color):self{
        return static::create([
            'owner_id'=>$owner->getKey(),
            'owner_type'=>get_class($owner),
            'group'=>$group,
            'text_color'=>$text_color,
            'bg_color'=>$background_color,
            'name'=>$name,
        ]);
    }
    public function scopeGroup($q,$group)
    {
        return $q->when($group!='',function($q) use ($group){
            $q->whereJsonContains('group',$group);
        })
        ->when($group=='',function($q){
            $q->whereJsonLength('group',0);
        });
    }

    public static function allGroups(){
        return static::all()->reduce(function($carry,$item){
            foreach($item->group as $group){
                if(!$carry->contains($group)){
                    $carry->push($group);
                }
                
            }
            return $carry;
        },collect([]))->mapWithKeys(fn($item)=>[$item=>$item]);
    }

    public function scopeForOwner($q,$owner){
        return $q->where('owner_id',$owner->getKey())->where('owner_type',get_class($owner));
    }

    public function scopeForOwnerClass($q,$class){
        return $q->where('owner_type',get_class($class));
    }
    public function scopeForOwnerClassAndId($q,$class,$id){
        return $q->where('owner_id',$id)->where('owner_type',$class);
    }

    public function scopeNotFinal($q){
        return $q->where('final',false);
    }


    public function scopeFinal($q){
        return $q->where('final',true);
    }
    public function scopeNotInitial($q){
        return $q->where('initial',false);
    }
    public function scopeInitial($q){
        return $q->where('initial',true);
    }

    public function scopeStarted($q){
        return $q->notFinal()->where('initial',false);
    }
}
