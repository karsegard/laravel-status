<?php

namespace KDA\Tests\Database\Factories;

use KDA\Tests\Models\PostWithoutTrait;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostWithoutTraitFactory extends Factory
{
    protected $model = PostWithoutTrait::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence(4),
        ];
    }
}
