<?php

namespace KDA\Tests\Database\Factories;

use KDA\Tests\Models\Blog;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogFactory extends Factory
{
    protected $model = Blog::class;

    public function definition()
    {
        return [
            'name' => $this->faker->sentence(4),
        ];
    }
}
