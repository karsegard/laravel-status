<?php

namespace KDA\Tests\Behat\Context;


/**
 * Defines application features from the specific context.
 */
class StatusContext extends BaseContext
{
    use Concerns\Factory;
    use Concerns\Models;
}
