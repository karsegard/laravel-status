<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Laravel\Status\Models\Concerns\HasStatus;
use KDA\Laravel\Status\Models\Contracts\StatusGroup;

class PostWithoutTrait extends Model 
{
   
    use HasFactory;

    protected $table="posts";
    protected $fillable = [
        'title'
    ];

    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostWithoutTraitFactory::new();
    }

    
}
