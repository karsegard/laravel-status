<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Laravel\Status\Models\Concerns\HasStatus;
use KDA\Laravel\Status\Models\Contracts\StatusGroup;

class PostWithNoStatusGroup extends Model 
{
   
    use HasFactory;
    use HasStatus;

    protected $table="posts";
    protected $fillable = [
        'title'
    ];

    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostFactory::new();
    }

    
}
