<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Laravel\Status\Models\Concerns\HasStatus;
use KDA\Laravel\Status\Models\Contracts\StatusGroup;

class Post extends Model implements StatusGroup
{
   
    use HasFactory;
    use HasStatus;

    protected $fillable = [
        'title'
    ];

    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostFactory::new();
    }

    //define default group status
    public function getStatusGroup():string{
        return 'post';
    }

    
}
