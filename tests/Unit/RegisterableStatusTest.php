<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Status\Facades\Status as FacadesStatus;
use KDA\Laravel\Status\Models\Status;
use KDA\Laravel\Status\Models\StatusHistory;
use KDA\Tests\Models\Blog;
use KDA\Tests\Models\Post;
use KDA\Tests\Models\PostWithNoStatusGroup;
use KDA\Tests\Models\PostWithoutTrait;
use KDA\Tests\TestCase;

class RegisterableStatusTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function registerable_model()
  {
    FacadesStatus::registerModel(PostWithoutTrait::class);
    $this->assertTrue(FacadesStatus::isRegisteredModel(PostWithoutTrait::class));

    $p = PostWithoutTrait::factory()->create()->load('statusHistory');
    $this->assertEquals(0,$p->statusHistory?->count());
    $this->assertNull(FacadesStatus::getStatus($p));

  }

  /** @test */
  function registerable_model_with_initial_status()
  {
    Status::factory()->create(['name'=>'draft','initial'=>true]);
    Status::factory()->create(['group'=>'blog','name'=>'draft','initial'=>true]);
    FacadesStatus::registerModel(PostWithoutTrait::class);
    $this->assertTrue(FacadesStatus::isRegisteredModel(PostWithoutTrait::class));

    $p = PostWithoutTrait::factory()->create();
    $this->assertEquals(1,$p->statusHistory->count());
    $this->assertNotNull(FacadesStatus::getStatus($p));

  }


  /** @test */
  function registerable_model_with_initial_status_group()
  {
    Status::factory()->create(['group'=>'invoice','name'=>'draft','initial'=>true]);
    Status::factory()->create(['group'=>'blog','name'=>'draft','initial'=>true]);
    FacadesStatus::registerModel(PostWithoutTrait::class,'blog');
    $this->assertTrue(FacadesStatus::isRegisteredModel(PostWithoutTrait::class));

    $p = PostWithoutTrait::factory()->create();
    $this->assertEquals(1,$p->statusHistory->count());
    $this->assertNotNull(FacadesStatus::getStatus($p));
   }

}