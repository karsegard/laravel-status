<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Status\Models\Status;
use KDA\Laravel\Status\Models\StatusHistory;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function can_create_status()
  {
    $o = Status::factory()->create([]);
    $this->assertNotNull($o);
  }


  /** @test */
  function can_create_status_history()
  {
    $o = StatusHistory::factory()->create([]);
    $this->assertNotNull($o);
  }




  
}