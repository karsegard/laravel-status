<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Status\Models\Status;
use KDA\Laravel\Status\Models\StatusHistory;
use KDA\Tests\Models\Blog;
use KDA\Tests\Models\Post;
use KDA\Tests\Models\PostWithNoStatusGroup;
use KDA\Tests\TestCase;

class StatusTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function group_scope()
  {
    Status::factory()->create(['group'=>'invoice']);
    Status::factory()->create(['group'=>'invoice']);
    Status::factory()->create([]);
    Status::factory()->create(['group'=>'invoice']);

    $this->assertEquals(Status::all()->count(),4);
    /*dump(Status::all());
    dump(Status::group('invoice')->get());*/
    $this->assertEquals(Status::group('invoice')->count(),3);
  }



  /** @test */
  function  initial_status_missing()
  {
    $p = Post::factory()->create();
    $this->assertNotNull($p);
    $this->assertEquals(0,StatusHistory::all()->count());
  }


  /** @test */
  function  model_with_no_status_group()
  {
    $p = PostWithNoStatusGroup::factory()->create();
    $this->assertNotNull($p);
    $this->assertEquals(0,StatusHistory::all()->count());
  }

  /** @test */
  function  initial_status()
  {
    
    Status::factory()->create(['group'=>'post','name'=>'published']);
    $initial = Status::factory()->create(['group'=>'post','name'=>'draft','initial'=>true]);

    $p = Post::factory()->create();
    $this->assertNotNull($p);
    $this->assertEquals(1,StatusHistory::all()->count());
    $this->assertNotNull($p->status);
    $this->assertEquals($p->status->getKey(),$initial->getKey());
  }


  /** @test */
  function  status_history()
  {
    
    $initial = Status::factory()->create(['group'=>'post','name'=>'draft','initial'=>true]);
    $another = Status::factory()->create(['group'=>'post','name'=>'published']);

    $p = Post::factory()->create();
    $this->assertNotNull($p);
    $this->assertEquals(1,StatusHistory::all()->count());
    $this->assertNotNull($p->status);
    $this->assertEquals($p->status->getKey(),$initial->getKey());
    $p->setStatus($another);

    $this->assertEquals(2,StatusHistory::all()->count());

    $this->assertNotNull($p->status);
    $this->assertEquals($p->fresh()->status->getKey(),$another->getKey());

  }


  /** @test */
  function  status_with_owner()
  {
    $blog = Blog::factory()->create();
    Status::factory()->create(['group'=>'post','name'=>'published','owner_id'=>$blog->getKey(),'owner_type'=>get_class($blog)]);
    

    $statuses = Status::forOwner($blog)->get();
    
    $this->assertEquals(1,$statuses->count());

  }

  /** @test */
  function duplicate_key_group_exception()
  {
    $this->expectException(\Exception::class);

    Status::factory()->create(['group'=>'post','name'=>'published']);
    Status::factory()->create(['group'=>'post','name'=>'published']);

  }


  /** @test */
  function duplicate_key_empty_group_exception()
  {
    $this->expectException(\Exception::class);

    Status::factory()->create(['name'=>'published']);
    Status::factory()->create(['name'=>'published']);

  }
  
  /** @test */
  function duplicate_key_different_group_no_exception()
  {

    $s1 = Status::factory()->create(['name'=>'published']);
    $s2 = Status::factory()->create(['name'=>'published','group'=>'post']);
    $this->assertNotNull($s1);
    $this->assertNotNull($s2);
  }
}