<?php

namespace KDA\Laravel\Status\Database\Factories;

use KDA\Laravel\Status\Models\StatusHistory;
use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Status\Models\Status;
use KDA\Tests\Models\User;
use KDA\Tests\Models\Post;

class StatusHistoryFactory extends Factory
{
    protected $model = StatusHistory::class;

    public function definition()
    {
        $model = Post::factory()->create();
        return [
            'status_id'=>Status::factory(),
            'user_id'=> User::factory(),
            'model_id'=>$model->getKey(),
            'model_type'=>get_class($model)
            //
        ];
    }
}
