<?php

namespace KDA\Laravel\Status\Database\Factories;

use KDA\Laravel\Status\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatusFactory extends Factory
{
    protected $model = Status::class;

    public function definition()
    {
        return [
            'name'=>$this->faker->word()
            //
        ];
    }
}
