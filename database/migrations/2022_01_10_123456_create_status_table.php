<?php
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use KDA\Laravel\Status\ServiceProvider;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create(ServiceProvider::getTableName('status'), function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->string('name');
            $table->text('group')->default(new Expression("('[]')"));
            $table->string('text_color')->nullable();
            $table->string('bg_color')->nullable();
            $table->boolean('final')->default(0);
            $table->boolean('initial')->default(0);
            $table->nullableNumericMorphs('owner');
            $table->text('meta')->nullable();
            $table->integer('sort')->nullable();
            $table->timestamps();
     //       $table->unique(['key','group']);
        });

        Schema::create(ServiceProvider::getTableName('histories'), function (Blueprint $table) {
            $table->id();
            $table->foreignId('status_id');
            $table->foreignId('user_id')->nullable();
            $table->morphs('model');
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(ServiceProvider::getTableName('status'));
        Schema::dropIfExists(ServiceProvider::getTableName('histories'));
     
        Schema::enableForeignKeyConstraints();
    }
};